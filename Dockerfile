FROM google/dart

WORKDIR /consul-do-assign-ip

ADD pubspec.* /consul-do-assign-ip/
RUN pub get
ADD . /consul-do-assign-ip
RUN pub get --offline

ADD entrypoint.sh /
RUN chmod +x /entrypoint.sh

ENV SLEEP_TIME 1

CMD []
ENTRYPOINT ["/entrypoint.sh"]