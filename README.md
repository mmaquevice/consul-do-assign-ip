# consul_do_assign_ip

A sample command-line application.

docker run -d --name assignip1 -e SERVICE_DOWN=HAPROXY_BACKUP -e SERVICE_UP=HAPROXY_MASTER -e FLOATING_IP=188.166.133.200 -e TOKEN=token -e CONSUL_URL=10.133.152.154 mmaquevice/consul-do-assign-ip:1.0

docker run -d --name assignip2 -e SERVICE_DOWN=HAPROXY_MASTER -e SERVICE_UP=HAPROXY_BACKUP -e FLOATING_IP=188.166.133.200 -e TOKEN=token -e CONSUL_URL=10.133.153.161 mmaquevice/consul-do-assign-ip:1.0