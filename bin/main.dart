import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:args/args.dart';
import 'package:stack_trace/stack_trace.dart';

Future assignIp(String token, String dropletId, String floatingIp) async {

  String apiBaseUrl = 'https://api.digitalocean.com/v2';

  String body = JSON.encode({
    'type': 'assign',
    'droplet_id': dropletId
  });

  http.Response response = await http.post(
      "${apiBaseUrl}/floating_ips/${floatingIp}/actions",
      headers: {
        'Authorization': 'Bearer ${token}',
        'Content-type': 'application/json'
      },
      body: body);
  String responseAssign = response.body;

  Map jsonAssign = JSON.decode(responseAssign);

  if (jsonAssign.keys.contains('message')) {
    print('${jsonAssign['id']}: ${jsonAssign['message']}');
  }

  print('Moving IP address: ${jsonAssign.toString()}');
}

Future<String> getDropletId() async {

  var urlId = "http://169.254.169.254/metadata/v1/id";
  http.Response response = await http.get(urlId);
  String dropletId = response.body;
  return dropletId;
}

Future<bool> hasFloatingIpAssigned() async {

  var urlFloatingIp =
  "http://169.254.169.254/metadata/v1/floating_ip/ipv4/active";
  http.Response response = await http.get(urlFloatingIp);
  bool hasFloatingIp = response.body.toLowerCase() == 'true';
  return hasFloatingIp;
}

Future<bool> isServiceUp(String service, String consulUrl) async {
  var urlConsul = "http://$consulUrl:8500/v1/catalog/service/${service}";
  http.Response response = await http.get(urlConsul);
  Map nodes = JSON.decode(response.body);
  return nodes.isNotEmpty;
}

Future<bool> hasToAssignIp(String serviceUp, String serviceDown, String consulUrl) async {

  bool hasFloatingIp = await hasFloatingIpAssigned();

  if(hasFloatingIp) {
    return false;
  }

  bool isServiceUpUp = await isServiceUp(serviceUp, consulUrl);
  bool isServiceDownUp = await isServiceUp(serviceDown, consulUrl);

  return isServiceUpUp && !isServiceDownUp;

}

main(List<String> arguments) async {

  Chain.capture(() async {

    final ArgParser argParser = new ArgParser()
      ..addOption('SERVICE_UP',
    abbr: 'u',
    help: "The SERVICE_UP need to be up to assign ip to this server.")
      ..addOption('SERVICE_DOWN',
    abbr: 'd',
    help: "The SERVICE_DOWN need to be down to assign ip to this server.")
      ..addOption('FLOATING_IP',
    abbr: 'f', help: "The FLOATING_IP to assign to this server.")
      ..addOption('TOKEN',
    abbr: 't', help: "The TOKEN to access DigitalOcean API.")
      ..addOption('CONSUL_URL',
    abbr: 'c', help: "The Consul Url.")
      ..addOption('SLEEP_TIME',
    abbr: 's', help: "The SLEEP_TIME in seconds before next check.");

    ArgResults argResults = argParser.parse(arguments);
    print(arguments.join(', '));
    print('Service to be up : ' + argResults['SERVICE_UP']);
    print('Service to be down : ' + argResults['SERVICE_DOWN']);
    print('Floating ip : ' + argResults['FLOATING_IP']);
    print('Token : ' + argResults['TOKEN']);
    print('Consul url : ' + argResults['CONSUL_URL']);

    print('Start checking services every ${argResults['SLEEP_TIME']} seconds...');

    while (true) {
      sleep(new Duration(seconds: int.parse(argResults['SLEEP_TIME'])));
      if (await hasToAssignIp(argResults['SERVICE_UP'], argResults['SERVICE_DOWN'], argResults['CONSUL_URL']).catchError((e) {
        print("Got error: ${e}");
        return false;
      })) {
        String dropletId = await getDropletId();
        assignIp(argResults['TOKEN'], dropletId, argResults['FLOATING_IP']).catchError((e) {
          print("Got error: ${e}");
        });
      }
    }
  });
}
