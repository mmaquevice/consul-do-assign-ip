#!/bin/bash

/usr/bin/dart bin/main.dart -u ${SERVICE_UP} -d ${SERVICE_DOWN} -f ${FLOATING_IP} -t ${TOKEN} -c ${CONSUL_URL} -s ${SLEEP_TIME}
